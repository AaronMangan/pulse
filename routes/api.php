<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Document;
use Illuminate\Container\RewindableGenerator;
use PHPUnit\Util\Annotation\DocBlock;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Returns the current version of the platform.
Route::middleware('auth:api')->get('/version', function(Request $request){
    return 'Development Alpha';
});

/**
 * Dashboard Route
 */

/**
 * Document Routes.
 */
// Returns a list of documents.
// Route::get('/index', function(Request $request){
//     return response()->json(Document::paginate(50));
// });
Route::get('/index', 'DocumentController@index');
Route::post('/documents', 'DocumentController@store');
Route::get('/documents/{id}', 'DocumentController@show');
Route::put('/documents/{id}', 'DocumentController@update');
Route::get('/edit-document/{id}', 'DocumentController@show');
Route::delete('delete-document/{id}', 'DocumentController@destroy');

/**
 * Revision Routes.
 */
Route::get('revisions', 'RevisionController@index')->name('revisions');
Route::get('/revisions/{id}', 'RevisionController@get');

/**
 * Status Routes
 */
Route::get('statuses', 'StatusController@index');
Route::get('/statuses/{id}', 'StatusController@get');

/**
 * Type Routes
 */
Route::get('types', 'TypeController@index');
Route::get('/types/{id}', 'TypeController@get');