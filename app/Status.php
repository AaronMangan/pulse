<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    // Define if timestamps are used for the model.
    public $timestamps = true;

    // Define the fillable array, allow these attributes to be mass-assigned.
    protected $fillable = ['code', 'name', 'status_notes'];

    // Protect these attributes by hiding them from serialisation.
    protected $hidden = [];

    // Define the visible array
    protected $visible = ['id', 'code', 'name', 'status_notes'];

    // One status may have many documents associated.
    public function documents()
    {
        return $this->hasMany('App\Document');
    }
}
