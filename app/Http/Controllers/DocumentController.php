<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Document;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Returns an index of documents.
        return Document::paginate(50);
        // return view('documents.index', compact($documents));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Store the document.
        $validation_rules = [
            'title' => 'required',
            'description' => 'nullable',
            'approved' => 'boolean',
            'revision' => 'required',
            'type' => 'required',
            'status' => 'required'
        ];
        $validator = Validator::make($request->all(), $validation_rules);
        if($validator->fails())
        {
            return response()->json($validator->errors());
        }
        $unique_code = uniqid();
        $doc = Document::create([
            'title' => $request->title,
            'description' => $request->description,
            'approved' => $request->approved,
            'revision_id' => $request->revision['value'],
            'type_id' => $request->type['value'],
            'status_id' => $request->status['value'],
            'company_id' => '1',
            'unique_id' => $unique_code
        ]);

        return response()->json([
            'status' => 'success',
            'message' => 'Document saved',
            'id' => $doc->id
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Fetch the document.
        return response()->json(Document::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // This is where the document is updated.
        $doc = Document::find($id);
        if(isset($doc))
        {
            // Store the document.
            $validation_rules = [
                'title' => 'required',
                'description' => 'nullable',
                'approved' => 'boolean',
                'revision' => 'required',
                'type' => 'required',
                'status' => 'required'
            ];
            
            $validator = Validator::make($request->all(), $validation_rules);
            if($validator->fails())
            {
                return response()->json($validator->errors());
            }
            
            $saved = $doc->update([
                'title' => $request->title,
                'description' => $request->description,
                'approved' => $request->approved,
                'revision_id' => $request->revision['value'],
                'type_id' => $request->type['value'],
                'status_id' => $request->status['value'],
                'company_id' => '1',
                'unique_id' => ''
            ]);
            
            if($saved)
            {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Document saved',
                    'id' => $doc->id
                ]);
            }
            else
            {
                return response()->json([
                    'status' => 'failed',
                    'message' => 'An error occured'
                ]);
            }
            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $d = Document::findOrFail($id);
        $d->delete();
        return response()->json([
            'status' => 'success',
            'message' => 'Document deleted' 
        ]);
    }
}
