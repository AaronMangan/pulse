<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    // Define timestamps for the model.
    public $timestamps = true;

    // Attributes that are mass assignable.
    protected $fillable = [
        'title', 'unique_code', 'description', 'approved', 'revision_id', 'type_id', 'status_id', 'company_id'
    ];

    // Assign which attributes are visible when serialised
    protected $visible = [
        'id', 'title', 'unique_code', 'description', 'approved', 'revision_id', 'type_id', 'status_id', 'company_id', 'created_at',
        'updated_at'
    ];

    // Create a one-to-many relationship between a document and a company.
    // a document can only belong to one company.
    public function company()
    {
        return $this->hasOne('App\Company');
    }
}
