<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Revision extends Model
{
    // Assign the use of timestamps for the model.
    public $timestamps = true;

    // Define the fillable attributes of this model.
    protected $fillable = [
        'code', 'description', 'revision_notes', 'company_id'
    ];

    // Assign any attributes to become visible when the model is serialised.
    protected $visible = [
        'id', 'code', 'description', 'revision_notes', 'company_id'
    ];

    // Assign any hidden attributes.
    protected $hidden = [];

}
