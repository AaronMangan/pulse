<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    // Timestamps
    public $timestamps = true;

    // Fillables.
    protected $fillable = [
        'name', 'address', 'phone', 'code', 'abn'
    ];

    // Hidden attributes.
    protected $hidden = [];

    // Attributes to always serialise (Show)
    protected $visible = [
        'name', 'address', 'phone', 'code', 'abn'
    ];

    // Relationships.
    
    /**
     * A company can have many documents.
     *
     * @return mixed App\Document
     */
    public function documents()
    {
        // A company may have many documents.
        return $this->hasMany('App\Document');
    }

    /**
     * A company may have many statuses.
     *
     * @return mixed App\Status
     */
    public function statuses()
    {
        return $this->hasMany('App\Status');
    }

    /**
     * A company may have many types.
     *
     * @return mixed App\Type
     */
    public function types()
    {
        return $this->hasMany('App\Type');
    }

    /**
     * A company has many revisions
     * 
     * @return mixed App\Revision
     */
    public function revisions()
    {
        return $this->hasMany('App\Revision');
    }
}
