<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    // Use timestamps for this model.
    public $timestamps = true;

    // Assign the attributes that are mass assignable.
    protected $fillable = [
        'name', 'code', 'description', 'comapny_id'
    ];

    // Define the visible attributes for the model.
    protected $visible = [
        'id', 'name', 'code', 'description', 'comapny_id'
    ];

    // Assign which attributes to hide when serialising.
    protected $hidden = [
        
    ];

    // Define the relationships.
    public function company()
    {
        return $this->hasOne('App\Company');
    }
}
