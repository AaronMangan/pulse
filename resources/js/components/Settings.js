import "bootstrap/dist/css/bootstrap.min.css";
import axios from 'axios'
import React, { Component } from 'react'
import NumericInput from 'react-numeric-input';

class Settings extends Component {
  constructor (props) {
    super(props)
    this.state = {
        page_results: 0
    }
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  /* Fetch data to view */
  componentDidMount() {
    /* Initialise tooltips */
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }
  /* Render */
  render () {
    return (
      <div className='container py-8'>
        <div className='row justify-content-center'>
          <div className='col-md-8'>
            <div className="card">
              <div className="card-header">Settings</div>
              <div className="card-body">
                <form action="">
                    <label>
                      Results per Page: 
                      <input
                        name="page_results"
                        type="number"
                        value={this.state.page_results}
                        onChange={this.handleInputChange} />
                    </label>
                    <label>
                      Results per Page: 
                      <input
                        name="page_results"
                        type="number"
                        value={this.state.page_results}
                        onChange={this.handleInputChange} />
                    </label>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Settings