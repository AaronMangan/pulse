import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import NewDocument from './NewDocument';
import DocumentIndex from './DocumentIndex';
import ViewDocument from './ViewDocument'
import Sidebar from './Sidebar';
import EditDocument from './EditDocument';
import Dashboard from './Dashboard';
import Settings from './Settings';


function App() {
    return (
      <BrowserRouter>
      <div>
        <Sidebar pageWrapId={"page-wrap"} outerContainerId={"App"} />
      </div>
          <div>
            <Switch>
              <Route exact path='/settings' component={Settings} />
              <Route exact path='/dashboard' component={Dashboard}/>
              <Route exact path='/create' component={NewDocument} />
              <Route exact path='/index' component={DocumentIndex} />
              <Route path="/documents/:id" component={ViewDocument} />
              <Route path='/edit-document/:id' component={EditDocument} />
            </Switch>
          </div>
      </BrowserRouter>
    );
}

export default App;

if (document.getElementById('app')) {
    ReactDOM.render(<App />, document.getElementById('app'));
}
