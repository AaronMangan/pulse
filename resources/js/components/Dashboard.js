import "bootstrap/dist/css/bootstrap.min.css";
import axios from 'axios'
import React, { Component } from 'react'

class Dashboard extends Component {
  constructor (props) {
    super(props)
    this.state = {
      term: '',
    }
  }

  /* Routes to the document index with a search term */
  searchForDocument(){
    alert("Serching");
  }

  /* Fetch data to view */
  componentDidMount() {
    /* Initialise tooltips */
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
    /* Document data */
    // var doc = await axios.get('/api/documents/' + this.props.match.params.id)
    // .then((response) => {
    //   return response.data;
    // })

    /* Revision */
    // var x = await axios.get('/api/documents/' + this.props.match.params.id)
    // .then((response) => {
    //   return axios.get('/api/revisions/' + response.data.revision_id);
    // })
    // .then((response) => {
    //   return response.data
    // });

    /* Type */
    // var type = await axios.get('/api/types/' + doc.type_id)
    // .then((response) => {
    //   return response.data;
    // })

    /* Status */
    // var stats = await axios.get('/api/statuses/' + doc.status_id)
    // .then((response) => {
    //   return response.data;
    // })

    // this.setState({
    //   status: stats,
    //   type: type,
    //   revision: x,
    //   document: doc
    // });
  }

  /* Render */
  render () {
    return (
      <div className='container py-8'>
        <div className='row justify-content-center'>
          <div className='col-md-8'>
            <div className="searchDiv">
                <form onSubmit={this.searchForDocument()}>          
                    <div className="input-group">
                        <input type="text" className="form-control" name="term" aria-label="Text input with dropdown button" />
                        <div className="input-group-append">
                          <button className="btn btn-primary" data-toggle="tooltip" data-placement="right" title="Search for documents" type="submit">Search</button>
                        </div>
                    </div>
                </form>
            </div>
            <div className="card-deck">
                <div className="card-body">
                  <h5 className="card-title">Card title</h5>
                  <p className="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                  <p className="card-text"><small className="text-muted">Last updated 3 mins ago</small></p>
                </div>
                <div className="card-body">
                  <h5 className="card-title">Card title</h5>
                  <p className="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                  <p className="card-text"><small className="text-muted">Last updated 3 mins ago</small></p>
                </div>
                <div className="card-body">
                  <h5 className="card-title">Card title</h5>
                  <p className="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                  <p className="card-text"><small className="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Dashboard