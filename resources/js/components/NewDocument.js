    import "bootstrap/dist/css/bootstrap.min.css";
    import axios from 'axios'
    import React, { Component } from 'react'
    import Select from 'react-select';
    import Switch from "react-switch";
   

    let options = [];
    let statusOptions = [];
    let typeOptions = [];
    
    class NewDocument extends Component {
      constructor (props) {
        super(props)
        this.state = {
          title: '',
          description: '',
          revisions: '',
          data: [],
          checked: false,
          errors: [],
          options: [],
          statusOptions: [],
          typeOptions: [],
          typeSelected: '',
          revSelected: '',
          statusSelected: '',
          checked: false,
        }
        // Handlers...
        this.handleFieldChange = this.handleFieldChange.bind(this)
        this.handleTypeSelectChange = this.handleTypeSelectChange.bind(this)
        this.handleRevSelectChange = this.handleRevSelectChange.bind(this)
        this.handleStatusSelectChange = this.handleStatusSelectChange.bind(this)
        this.handleCreateNewDocument = this.handleCreateNewDocument.bind(this)
        this.hasErrorFor = this.hasErrorFor.bind(this)
        this.renderErrorFor = this.renderErrorFor.bind(this)
        this.handleApprovedChange = this.handleApprovedChange.bind(this);
      }

      /* Handlers */
      handleFieldChange (event) {
        this.setState({
          [event.target.name]: event.target.value
        })
      }

      handleCreateNewDocument (event) {
        event.preventDefault()

        const { history } = this.props

        const document = {
          title: this.state.title,
          description: this.state.description,
          revision: this.state.revSelected,
          approved: this.state.checked,
          type: this.state.typeSelected,
          status: this.state.statusSelected
        }

        axios.post('/api/documents', document)
          .then(response => {
            // redirect to the homepage
            history.push('/index')
          })
          .catch(error => {
            this.setState({
              errors: error.response.errors
            })
          })
      }

      handleTypeSelectChange (typeSelected) {
        this.setState({ typeSelected });
      }

      handleStatusSelectChange (statusSelected) {
        this.setState({ statusSelected });
      }

      handleRevSelectChange (revSelected) {
        this.setState({ revSelected });
      }

      /* Errors */
      hasErrorFor (field) {
        return !!this.state.errors[field]
      }

      // Revisions
      async copyOptionsForAsync() {
        let response = await fetch('/api/revisions');
        let data = await response.json();
        data.forEach(element => {
          let dropDownEle = { label: element.code, value: element.id };
          options.push(dropDownEle);
        });
      }

      // Status options
      async statusOptionsForAsync() {
        let response = await fetch('/api/statuses');
        let data = await response.json();
        data.forEach(element => {
          let dropDownEle = { label: element.name, value: element.id };
          statusOptions.push(dropDownEle);
        });
      }

      // Type options
      async typeOptionsForAsync() {
        let response = await fetch('/api/types');
        let data = await response.json();
        data.forEach(element => {
          let dropDownEle = { label: element.name + " (" + element.code + ")", value: element.id };
          typeOptions.push(dropDownEle);
        });
      }

      handleApprovedChange(checked) {
        this.setState({ checked });
      }

      renderErrorFor (field) {
        if (this.hasErrorFor(field)) {
          return (
            <span className='invalid-feedback'>
              <strong>{this.state.errors[field][0]}</strong>
            </span>
          )
        }
      }

      /* System */
      componentDidMount() {
        // Get options for revision.
        this.copyOptionsForAsync();

        // Get options for statuses.
        this.statusOptionsForAsync();

        // Get options for types.
        this.typeOptionsForAsync();
      }

      render () {
        const { selectedOption } = this.state;
        return (
          <div className='container py-8'>
            <div className='row justify-content-center'>
              <div className='col-md-8'>
                <div className='card'>
                  <div className='card-header'>Create New Document</div>
                  <div className='card-body'>
                    <form onSubmit={this.handleCreateNewDocument}>
                      
                      {/* Title */}
                      <div className='form-group'>
                        <label htmlFor='title'>Document Title</label>
                        <input
                          id='title'
                          type='text'
                          className={`form-control ${this.hasErrorFor('title') ? 'is-invalid' : ''}`}
                          name='title'
                          value={this.state.title}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor('title')}
                      </div>
                      
                      {/* Description */}
                      <div className='form-group'>
                        <label htmlFor='description'>Description</label>
                        <textarea
                          id='description'
                          className={`form-control ${this.hasErrorFor('description') ? 'is-invalid' : ''}`}
                          name='description'
                          rows='10'
                          value={this.state.description}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor('description')}
                      </div>

                      {/* Approved Switch */}
                      <div className="row">
                        <div className="col-md-6">
                          <div className='form-group'>
                          <label>
                            <span>Approved</span>
                            <Switch
                              id="approved"
                              name="approved"
                              onChange={this.handleApprovedChange}
                              checked={this.state.checked}
                              className="react-switch"
                              onColor="#dca9f5"
                              onHandleColor="#952cd1"
                              offColor="#bdb8bf"
                              offHandleColor="#952cd1"
                              handleDiameter={20}
                              uncheckedIcon={false}
                              checkedIcon={false}
                              boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
                              activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
                              height={20}
                              width={48}
                            />
                          </label>
                            {this.renderErrorFor('approved')}
                          </div>
                        </div>


                        {/* Type Select */}
                        <div className="col-md-6">
                          <div className='form-group'>
                            <label htmlFor='type_id'>Type</label>
                            <div>
                              <Select
                                name="type"
                                id="type"
                                required
                                className="react-select"
                                value={selectedOption}
                                onChange={this.handleTypeSelectChange}
                                options={typeOptions}
                                theme={theme => ({
                                  ...theme,
                                  borderRadius: 10,
                                  colors: {
                                    ...theme.colors,
                                    primary25: '#dca9f5',
                                    primary: 'black',
                                  },
                                })}
                                placeholder="Please Select a Type..."
                              />
                            </div>
                            {this.renderErrorFor('types')}
                          </div>
                        </div>
                      </div>
                      <div className="row">

                        {/* Revision Select */}
                        <div className="col-md-6">
                          <div className='form-group'>
                            <label htmlFor='revision_id'>Revision</label>
                            <div>
                              <Select
                                name="revisions"
                                id="revisions"
                                required
                                value={selectedOption}
                                onChange={this.handleRevSelectChange}
                                options={options}
                                placeholder="Please Select..."
                                theme={theme => ({
                                  ...theme,
                                  borderRadius: 10,
                                  colors: {
                                    ...theme.colors,
                                    primary25: '#dca9f5',
                                    primary: 'black',
                                  },
                                })}
                              />
                            </div>
                            {this.renderErrorFor('revision')}
                          </div>
                        </div>

                        {/* Status Select */}
                        <div className="col-md-6">
                          <div className='form-group'>
                            <label htmlFor='status_id'>Status</label>
                            <div>
                              <Select
                                name="statuses"
                                value={selectedOption}
                                onChange={this.handleStatusSelectChange}
                                options={statusOptions}
                                placeholder="Please Select a Status..."
                                required
                                theme={theme => ({
                                  ...theme,
                                  borderRadius: 10,
                                  colors: {
                                    ...theme.colors,
                                    primary25: '#dca9f5',
                                    primary: 'black',
                                  },
                                })}
                              />
                            </div>
                            {this.renderErrorFor('statuses')}
                          </div>
                        </div>
                      </div>

                      {/* Create Button */}
                      <button className='btn btn-primary'>Create</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )
      }
    }

    export default NewDocument