    import "bootstrap/dist/css/bootstrap.min.css";
    import axios from 'axios'
    import React, { Component } from 'react'
    import { Link } from 'react-router-dom'
    import  BootBox  from  'bootbox-react';
    import { Redirect } from 'react-router';

    class ViewDocument extends Component {
      constructor (props) {
        super(props)
        this.state = {
          isLoading: true,
          document: [],
          type: [],
          revision: [],
          status: [],
          approved: '',
          id: null,
          show: false
        }
      }
      
      /* Fetch data to view */
      async componentDidMount() {
        /* Document data */
        var doc = await axios.get('/api/documents/' + this.props.match.params.id)
        .then((response) => {
          return response.data;
        })

        /* Revision */
        var x = await axios.get('/api/documents/' + this.props.match.params.id)
        .then((response) => {
          return axios.get('/api/revisions/' + response.data.revision_id);
        })
        .then((response) => {
          return response.data
        });

        /* Type */
        var type = await axios.get('/api/types/' + doc.type_id)
        .then((response) => {
          return response.data;
        })

        /* Status */
        var stats = await axios.get('/api/statuses/' + doc.status_id)
        .then((response) => {
          return response.data;
        })
        this.setState({
          status: stats,
          type: type,
          revision: x,
          document: doc,
          id: doc.id
        });
        this.checkApproved();
      }

      /* Assigns the 'Approved' badge and Approval status column*/
      checkApproved(){
        var app = this.state.document.approved
        if(app === 1)
        {
          this.setState({approved: 'Approved'});
          return 'Approved';
        }
        else
        {
          this.setState({approved: 'Unapproved'});
          return '';
        }
      }

      /* Delete the document */
      deleteDocument(){
        alert(_id);
        // axios.delete(`/api/delete-document/${this.document.id}`, function(result){
        //   console.log(result);
        // });
      }
    
      handleClose(){
        // setState({show:false})
      }
      /* Render */
      render () {
        return (
          <div className='container py-8'>
            <div className='row justify-content-center'>
              <div className='col-md-8'>
                <div className='card'>
                  <div className='card-header'>View Document 
                      <span className='badge badge-primary badge-pill'>
                         {this.state.approved}
                      </span>
                  </div>
                  <div className='card-body'>
                    <dl className="row">
                      <dt className="col-sm-3">Title</dt>
                      <dd className="col-sm-9">{this.state.document.title}</dd>
          
                      <dt className="col-sm-3">Description</dt>
                      <dd className="col-sm-9">
                        <p>{this.state.document.description}</p>
                      </dd>
          
                      <dt className="col-sm-3">Revision</dt>
                      <dd className="col-sm-9">{this.state.revision.code}</dd>
          
                      <dt className="col-sm-3">Type</dt>
                      <dd className="col-sm-9">{this.state.type.code} - {this.state.type.name}</dd>
          
                      <dt className="col-sm-3">Status</dt>
                      <dd className="col-sm-9">{this.state.status.name}</dd>

                      <dt className="col-sm-3">Approval Status</dt>
                      <dd className="col-sm-9">{this.state.approved}</dd>
                    </dl>
                  </div>
                </div>
                <div className="card">
                    <div className="card-header">Actions</div>
                    <div className="card-body">
                      <Link
                          className='btn btn-primary'
                          to={`/edit-document/${this.props.match.params.id}`}
                          key={document.id}
                        >Edit this Document</Link>
                      <div className="d-inline-flex p-2"></div>
                      <button className="btn btn-warning"
                        onClick={()=>{this.setState({show:true})}}>Delete This Document</button>
                      <BootBox
                        type='confirm'
                        message="Do you want to Continue?"
                        show={this.state.show} 
                        onSuccess={()=>{
                          axios.delete(`/api/delete-document/${this.props.match.params.id}`)
                          this.props.history.push("/index"); 
                        }}
                        onCancel={this.handleClose}
                        />
                    </div>
                </div>
              </div>
            </div>
          </div>
        )
      }
    }

    export default ViewDocument