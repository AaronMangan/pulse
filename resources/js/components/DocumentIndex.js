import axios from 'axios'
    import React, { Component } from 'react'
    import { Link } from 'react-router-dom'
    import ReactPaginate from 'react-paginate'

    class DocumentIndex extends Component {
      constructor () {
        super()
        this.state = {
          documents: [],
          currentPage: '',
          createdAt: '',
          currentDate: new Date(),
          show: false,
        }
        this.handlePageClick = this.handlePageClick.bind(this);
      }

      async componentDidMount () {
        
        axios.get('/api/index').then(response => {
          this.setState({
            documents: response.data.data
          })
        })
        const page = this.getQueryStringValue('page');
		    await Promise.resolve(
		    	this.setState(() => ({ currentPage: page ? page : 1 }))
		    );

		    this.getDocumentData();
      }

      getQueryStringValue(key) {
        const value = decodeURIComponent(
          window.location.search.replace(
            new RegExp(
              '^(?:.*[&\\?]' +
                encodeURIComponent(key).replace(/[\.\+\*]/g, '\\$&') +
                '(?:\\=([^&]*))?)?.*$',
              'i'
            ),
            '$1'
          )
        );
        return value ? value : null;
      }
      
      async handlePageClick(data) {
        const page = data.selected >= 0 ? data.selected + 1 : 0;
        await Promise.resolve(this.setState(() => ({ currentPage: page })));
    
        this.getDocumentData();
      }
    
      async getDocumentData() {
    
        if (history.pushState) {
          const newUrl =
            window.location.protocol +
            '//' +
            window.location.host +
            window.location.pathname +
            '?page=' +
            this.state.currentPage;
          window.history.pushState({ path: newUrl }, '', newUrl);
    
          const response = await axios.get(newUrl);
    
          try {
            if (response.data.status == 'success') {
              this.setState(() => ({
                documents: response.data.documents.data,
                currentPage: response.data.documents.current_page,
                pageCount: response.data.data.documents.last_page
              }));
              window.scrollTo(0, 0);
            } else {
              
            }
          } catch (error) {
            console.log(error);
          }
        }
      }

      checkCreatedData(document){
        var created = new Date(document);
        var current = new Date(this.state.currentDate);
        var date_difference=(current - created)/(24*3600*1000);
        if (date_difference > 1) {
          return '';
        }
        else {
          return 'new';
        }
      }

      componentWillUnmount() {
        // Fix Warning: Can't perform a React state update on an unmounted component
        this.setState = (state,callback)=>{
            return;
        };
      }

      render () {
        const { documents } = this.state
        return (
          <div className='container py-4'>
            <div className='row justify-content-center'>
              <div className='col-md-12'>
                <div className='card'>
                  <div className='card-header'>All Documents</div>
                  <div className='card-body'>
                    <ul className='list-group list-group-flush'>
                      {documents.map(document => (
                        <Link
                          className='list-group-item list-group-item-action d-flex justify-content-between align-items-center'
                          to={`documents/${document.id}`}
                          key={document.id}
                        >
                          {document.title}
                          <span className='badge badge-primary badge-pill'>
                            {this.checkCreatedData(document.created_at)}
                          </span>
                        </Link>
                      ))}
                    </ul>
                  </div>
                  <div>
                    </div>
                  <ReactPaginate
				          	pageCount={this.state.pageCount}
				          	initialPage={this.state.currentPage - 1}
				          	forcePage={this.state.currentPage - 1}
				          	pageRangeDisplayed={4}
				          	marginPagesDisplayed={2}
				          	previousLabel="&#x276E;"
				          	nextLabel="&#x276F;"
				          	containerClassName="pagination flex-center"
				          	activeClassName="active"
				          	disabledClassName="disabled"
				          	onPageChange={this.handlePageClick}
                    disableInitialCallback={true}
				          />
                </div>
              </div>
            </div>
          </div>
        )
      }
    }

    export default DocumentIndex