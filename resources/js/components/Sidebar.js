import React from "react";
import { slide as Menu } from "react-burger-menu";

export default props => {
  return (
    <Menu {...props}>
      <a className="menu-item" href="/dashboard">
        Home
      </a>

      <a className="menu-item" href="/index">
        Documents
      </a>

      <a className="menu-item" href="/create">
        Create a Document
      </a>
      
      <a className="menu-item" href="/settings">
        Settings
      </a>

      <a className="menu-item" href="/logout">
        Logout
      </a>
    </Menu>
  );
};