<?php

use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seed at least one company to use.
        DB::table('companies')->insert([
            'name' => 'Pulse Document Management Systems',
            'address' => '255 Main Street, Golidaine',
            'phone' => '1800789987',
            'code' => 'a9G=jv2$2',
            'abn' => ''
        ]);
    }
}