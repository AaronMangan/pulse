<?php

use Illuminate\Database\Seeder;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seed some Types in the database.
        DB::table('types')->insert([
            'name' => 'Report',
            'code' => 'RPT',
            'description' => 'For reports of any kind'
        ]);
        DB::table('types')->insert([
            'name' => 'Drawing',
            'code' => 'DWG',
            'description' => 'For drawings of any kind'
        ]);
        DB::table('types')->insert([
            'name' => 'Contract',
            'code' => 'CON',
            'description' => 'For contracts and contract attachments, etc'
        ]);
        DB::table('types')->insert([
            'name' => 'Register',
            'code' => 'REG',
            'description' => 'Registers, for example, an Equipment Register'
        ]);
        DB::table('types')->insert([
            'name' => 'Data Sheet',
            'code' => 'DAT',
            'description' => 'Data Sheets hold technical information about a piece of equipment or product'
        ]);
        DB::table('types')->insert([
            'name' => 'Certificate',
            'code' => 'CERT',
            'description' => 'A certificate that provides evidence of a particular'
        ]);
        DB::table('types')->insert([
            'name' => 'Form',
            'code' => 'FRM',
            'description' => 'A form that is filled out, such as an application'
        ]);
        DB::table('types')->insert([
            'name' => 'Invoice',
            'code' => 'INV',
            'description' => 'An invoice that request payment for goods or services'
        ]);
        DB::table('types')->insert([
            'name' => 'Quote',
            'code' => 'QUOT',
            'description' => 'A quote requesting pricing for fixed terms'
        ]);
        DB::table('types')->insert([
            'name' => 'Diagram',
            'code' => 'DIAG',
            'description' => 'A Diagram outlines or illustrates a concept, process, artifact, interface etc'
        ]);
        DB::table('types')->insert([
            'name' => 'Manual',
            'code' => 'MANU',
            'description' => 'Describes how something is performed or undertaken'
        ]);
        DB::table('types')->insert([
            'name' => 'Article',
            'code' => 'ARTI',
            'description' => 'An information piece to convey information'
        ]);
        DB::table('types')->insert([
            'name' => 'Bill of Materials',
            'code' => 'BILL',
            'description' => 'A List of materials, usually required for construction or assembly'
        ]);
        DB::table('types')->insert([
            'name' => 'Letter',
            'code' => 'LETT',
            'description' => 'A written piece of correspondence'
        ]);
        // DB::table('types')->insert([
        //     'name' => '',
        //     'code' => '',
        //     'description' => ''
        // ]);
    }
}