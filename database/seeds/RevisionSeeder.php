<?php

use Illuminate\Database\Seeder;

class RevisionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seed revision in the database.
        DB::table('revisions')->insert([
            'code' => 'A',
            'description' => 'Alphabetic revision A for unapproved documents',
            'revision_notes' => 'Notes for revision go here.',
            'company_id' => '1'
        ]);

        DB::table('revisions')->insert([
            'code' => 'B',
            'description' => 'Alphabetic revision B for unapproved documents',
            'revision_notes' => 'Notes for revision go here.',
            'company_id' => '1'
        ]);

        DB::table('revisions')->insert([
            'code' => 'C',
            'description' => 'Alphabetic revision c for unapproved documents',
            'revision_notes' => 'Notes for revision go here.',
            'company_id' => '1'
        ]);
        
        DB::table('revisions')->insert([
            'code' => '0',
            'description' => 'Numeric revision 0 for approved documents',
            'revision_notes' => 'Notes for revision go here.',
            'company_id' => '1'
        ]);
        DB::table('revisions')->insert([
            'code' => '1',
            'description' => 'Numeric revision 1 for approved documents',
            'revision_notes' => 'Notes for revision go here.',
            'company_id' => '1'
        ]);
        DB::table('revisions')->insert([
            'code' => '2',
            'description' => 'Numeric revision 2 for approved documents',
            'revision_notes' => 'Notes for revision go here.',
            'company_id' => '1'
        ]);
    }
}