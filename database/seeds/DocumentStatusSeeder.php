<?php

use Illuminate\Database\Seeder;

class DocumentStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seed some statuses in the DB.
        DB::table('statuses')->insert([
            'name' => 'DRAFT',
            'code' => 'DRAFT',
            'status_notes' => 'A draft document, not yet completed.'
        ]);
        DB::table('statuses')->insert([
            'name' => 'UNDER REVIEW',
            'code' => 'REVIEW',
            'status_notes' => 'The document is under review awaiting approval or feedback.'
        ]);
        DB::table('statuses')->insert([
            'name' => 'APPROVED',
            'code' => 'APPR',
            'status_notes' => 'The document is approved for its intended purpose.'
        ]);
        DB::table('statuses')->insert([
            'name' => 'ISSUED FOR USE',
            'code' => 'USE',
            'status_notes' => 'Document has been issued to another party for its intended use.'
        ]);
        DB::table('statuses')->insert([
            'name' => 'ISSUED FOR CONSTRUCTION',
            'code' => 'IFC',
            'status_notes' => 'Usually a drawing or construction document, has been approved to construct to the dimensions it contains'
        ]);
        DB::table('statuses')->insert([
            'name' => 'OBSOLETE',
            'code' => 'OBS',
            'status_notes' => 'Document is no longer fit for purpose. Retained for historical information'
        ]);
        DB::table('statuses')->insert([
            'name' => 'SUPERSEDED',
            'code' => 'SUP',
            'status_notes' => 'Another document has replaced this one, or another revision. Retained for information.'
        ]);
        DB::table('statuses')->insert([
            'name' => 'ISSUED FOR INFORMATION',
            'code' => 'IFI',
            'status_notes' => 'Issued to another party for the purposes of informing them.'
        ]);
        DB::table('statuses')->insert([
            'name' => 'CANCELLED',
            'code' => 'CAN',
            'status_notes' => 'Document was stopped being prepared before it was approved.'
        ]);
        DB::table('statuses')->insert([
            'name' => 'ISSUED FOR TENDER',
            'code' => 'IFT',
            'status_notes' => 'Documents issued for the purposes of tendering but may be revised.'
        ]);
        DB::table('statuses')->insert([
            'name' => 'ISSUED FOR COMMENT',
            'code' => 'COMM',
            'status_notes' => 'Document has being issued to another party for comments'
        ]);
        // DB::table('statuses')->insert([
        //     'name' => '',
        //     'code' => '',
        //     'status_notes' => ''
        // ]);
        
    }
}
