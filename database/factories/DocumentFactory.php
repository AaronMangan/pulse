<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Document;
use Faker\Generator as Faker;

$factory->define(Document::class, function (Faker $faker) {
    $title = $faker->sentence;
    return [
        // Define attributes for documents
        'title' => $title,
        'unique_code' => $faker->bothify($title),
        'description' => $faker->sentence,
        'approved' => $faker->randomElement(['0', '1']),
        'revision_id' => '1',
        'type_id' => '1',
        'status_id' => '1',
        'company_id' => '1'
    ];
});